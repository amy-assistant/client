export default socket => store => {
    if (store.getters.online) socket.connect()

    store.subscribe((mutation, state) => {
        if (mutation.type == 'changeOnline') {
            if (state.online) {
                socket.connect()
                store.dispatch('auth/authenticate')
            }
            else socket.disconnect()
        }
    })

    socket.on("connect_error", () => {
        store.commit("changeOnline", false)
    })
    socket.on("connection", () => {
        store.commit("changeOnline", true)
    })
    window.addEventListener('online', () => {
        store.commit("changeOnline", true)
    })
    window.addEventListener('offline', () => {
        store.commit("changeOnline", false)
    })
}
