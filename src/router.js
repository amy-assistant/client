import Vue from 'vue'

import store from './store'

import Router from 'vue-router'
import {
  Overview,
  Home,
  Chat,
  Settings,
  Login,
  Contacts,
  Account,
  SettingsNotifications,
  Colors,
  Help,
  BlockedContacts,
  LogedOut,
  Start,
  Register,
  AccountAuth,
  ChatSettings,
  ContactSettings
} from '@/views'
import { ChatSettingButton } from './components';


Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/start',
      component: LogedOut,
      meta: {
        public: true,
        onlyWhenLoggedOut: true
      },
      children: [{
          path: '',
          component: Start,
          name: 'Start',
          meta: {
            name: 'Amy Assistant',
            noback: true,
            public: true,
            onlyWhenLoggedOut: true
          }
        },
        {
          name: 'Login',
          path: '/login',
          component: Login,
          meta: {
            public: true,
            onlyWhenLoggedOut: true
          }
        },
        {
          name: 'Register',
          path: '/register',
          component: Register,
          meta: {
            public: true,
            onlyWhenLoggedOut: true
          }
        }
      ]
    }, {
      path: '/',
      component: Overview,
      children: [{
          path: '',
          name: 'Amy Assistant',
          component: Home,
          meta: {
            right: '',
            position: 'middle',
            noback: true
          }
        }, {
          path: '/contacts',
          name: 'Contacts',
          component: Contacts,
          meta: {
            position: 'right',
            noback: true
          }
        },
        {
          component: ContactSettings,
          path: '/contacts/:id/settings',
          name: 'contactSettings',
          meta: {
            name: 'Contact Settings',
            position: 'right'
          }
        },
        {
          path: '/settings',
          name: 'Settings',
          component: Settings,
          meta: {
            position: 'left',
            noback: true
          }
        },
        {
          component: Account,
          path: '/settings/account',
          name: 'Account'
        },
        {
          component: AccountAuth,
          path: '/settings/account/messenger',
          name: 'messenger',
        },
        {
          component: AccountAuth,
          path: '/settings/account/telegram',
          name: 'telegram',
        },
        {
          component: SettingsNotifications,
          path: '/settings/notifications',
          name: 'Notifications'
        },
        {
          component: Help,
          path: '/settings/help',
          name: 'Help'
        },
        {
          component: BlockedContacts,
          path: '/settings/blockedcontacts',
          name: 'Blocked Contacts'
        },
        {
          component: Colors,
          path: '/settings/colors',
          name: 'Colors'
        }
      ]
    },
    {
      component: Chat,
      path: '/chat/:id',
      name: 'chat',
      meta: {
        right: ChatSettingButton,
      }
    },
    {
      component: ChatSettings,
      path: '/chat/:id/settings',
      name: 'chatSettings',
      meta: {
        name: 'Chat Settings',
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  to.meta.name = to.name
  if (to.name == 'Blank') {
    to.meta.name = store.getters.getChannelName(to.params.id)
    to.meta.right = 'Hidden'
  }
  next()
})



router.beforeEach(async (to, from, next) => {
  const isPublic = to.matched.some(record => record.meta.public)
  const onlyWhenLoggedOut = to.matched.some(record => record.meta.onlyWhenLoggedOut)

  const loggedIn = !!store.state.auth.user;

  if (to.fullPath == '/' && !loggedIn) {
    return next('/start')
  }

  if (!isPublic && !loggedIn) {
    return next({
      path: '/login',
      query: {
        redirect: to.fullPath
      } // Store the full path to redirect the user to after login
    });
  }

  // Do not allow user to visit login page or register page if they are logged in
  if (loggedIn && onlyWhenLoggedOut) {
    return next('/')
  }

  next();
})

export default router;