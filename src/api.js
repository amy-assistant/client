import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'
import auth from '@feathersjs/authentication-client'
import io from 'socket.io-client'



const socket = io(process.env.VUE_APP_API_SERVER || 'http://localhost:3030', {
    transports: ['websocket'],
    autoConnect: false
})



const feathersClient = feathers()
    .configure(socketio(socket, {
        timeout: 15000
    }))
    .configure(auth({
        storage: window.localStorage
    }))

feathersClient.hooks({
    before: {
        all: [
            // context => {
            //     /* eslint-disable no-console */
            //     console.log(context)
            //     return context
            // }
        ]
    }
})

export default feathersClient