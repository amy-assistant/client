import Vue from 'vue'
import './plugins/fontawesome'
import './plugins/chatScroll'
import './registerServiceWorker'
import router from './router'
import store from './store'
import App from './App.vue'


Vue.config.productionTip = false

function init() {
  new Vue({
    router,
    store,
    render: h => h(App)
  }).$mount('#app')
}


if (!store.getters.online) init()
else if (!localStorage.getItem('feathers-jwt')) init()
else store.dispatch('auth/authenticate').then(init).catch(init)



