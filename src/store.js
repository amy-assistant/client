import Vue from "vue";
import Vuex from "vuex";
// import createPersistedState from 'vuex-persistedstate'

import feathersVuex from "feathers-vuex";
import feathersClient from "./api";
import onlinePlugin from "./online";

const { service, auth, FeathersVuex } = feathersVuex(feathersClient, {
  idField: "_id"
});

Vue.use(Vuex);
Vue.use(FeathersVuex);

feathersClient.service("plugins").hooks({
  after: {
    patch: [
      ({ service, result }) => {
        if (result.status) {
          if (result.status == 202) {
            service.Model.store.commit("setPluginAuthState", 3);
          } else if (result.status == 201) {
            service.Model.store.commit("setPluginAuthState", 2);
          } else if (result.status == 401) {
            service.Model.store.commit("setPluginAuthState", 2);
          } else {
            service.Model.store.commit("setPluginAuthState", 1);
          }
          service.Model.store.commit("setPluginAuthMessage", result);
        }
      }
    ]
  }
});

export default new Vuex.Store({
  plugins: [
    onlinePlugin(feathersClient.io),
    service("messages"),
    service("users"),
    service("channels"),
    service("plugins"),
    service("contacts"),
    auth({
      userService: "users"
    })
  ],
  state: {
    online: navigator.onLine,
    pluginAuthMessage: "",
    pluginAuthState: 0
  },
  getters: {
    online: state => state.online,
    pluginAuthMessage: state => state.pluginAuthMessage,
    pluginAuthState: state => state.pluginAuthState
  },
  mutations: {
    changeOnline(state, val) {
      state.online = val;
    },
    setPluginAuthMessage(state, val) {
      state.pluginAuthMessage = val;
    },
    setPluginAuthState(state, val) {
      state.pluginAuthState = val;
    }
  },
  actions: {}
});
