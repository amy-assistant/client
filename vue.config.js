// const fs = require('fs');

module.exports = {
    chainWebpack: (config) => {
        const svgRule = config.module.rule('svg');

        svgRule.uses.clear();

        svgRule
            .use('vue-svg-loader')
            .loader('vue-svg-loader');
    },
    css: {
        loaderOptions: {
            sass: {
                data: `@import "@/styles/style.scss";`
            }
        }
    },
    // devServer: {
    //     open: process.platform === 'darwin',
    //     host: 'localhost',
    //     port: 8085, // CHANGE YOUR PORT HERE!,
    //     https: {
    //         key: fs.readFileSync('./cert/server.key'),
    //         cert: fs.readFileSync('./cert/server.crt'),
    //         ca: fs.readFileSync('./cert/rootCA.pem'),
    //     }
    // },
    pwa: {
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            swSrc: 'public/service-worker.js',
            importWorkboxFrom: 'local',
        },
    },
};