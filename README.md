# Amy Client

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn run serve
```

### Compiles and minifies for production

```
yarn run build
```

### Lints and fixes files

```
yarn run lint
```

### Docker build

```
docker build -t amy_client .
```

### Docker run

```
docker run  -dp 8080:80 amy_client
```

> Amy client is now live at `localhost:8080`
