PORTS ?= -p 8080:80
IMAGE_NAME ?= registry.gitlab.com/amy-assistant/client
CONTAINER_NAME ?= amy-client

build: Dockerfile
	docker build --no-cache -t $(IMAGE_NAME) .

push:
	docker push $(IMAGE_NAME)

run:
	docker run --rm -it --name $(CONTAINER_NAME) $(PORTS) $(IMAGE_NAME)

start:
	docker run -d --name $(CONTAINER_NAME) $(PORTS) $(IMAGE_NAME)

stop:
	docker stop $(CONTAINER_NAME)

shell:
	docker exec -it $(CONTAINER_NAME) sh

rm:
	docker rm $(CONTAINER_NAME)