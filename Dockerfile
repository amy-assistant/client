FROM node:lts-alpine as build

# RUN apk add python

WORKDIR /app

ENV VUE_APP_API_SERVER=http://159.89.24.207:3030

COPY . .

RUN yarn install

RUN yarn build

FROM socialengine/nginx-spa as serve

COPY --from=build /app/dist /app
