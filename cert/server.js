const path = require("path");
const fs = require("fs");
const express = require("express");
const https = require("https");
const certOptions = {
    key: fs.readFileSync(path.resolve("cert/server.key")),
    cert: fs.readFileSync(path.resolve("cert/server.crt"))
};
const app = express();



app.get('/data', (req, res) => {
    res.send("Hey! It Works!")
});

// app.use('/', express.static(path.join(__dirname, 'dist')))

app.use('/', express.static(path.join(__dirname, '../dist')))

// app.get('*', (req, res) => {
//     res.send("Nothing here!")
// });
https.createServer(certOptions, app).listen(8443);

// console.log('server listen')